const mongoose = require('mongoose')

const Flat = mongoose.model('Flat', { // 2
  title: {           // 3
    type: String,
    required: true,
    minlength: 1
  },
  description: { // 4
    type: String,
    required: true,
    minlength: 1
  },
  price: { // 5
    type: String,
    required: true,
    minlength: 1
  }
  owner: { // 5
    type: String,
    required: true,
    minlength: 1
  }
  phone: { // 5
    type: String,
    required: true,
    minlength: 1
  }
  isFlat: { // 5
    type: String,
    required: true,
    minlength: 1
  }
  rooms: { // 5
    type: Number,
    required: true,
    minlength: 1
  }
})
 
module.exports = {Flat}
