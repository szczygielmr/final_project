const Jsdom = require('jsdom').JSDOM
const fetch = require('node-fetch')
const express = require('express')
const r = require('rethinkdb')

const cracowBaseUrl = 'https://www.gumtree.pl/s-mieszkania-i-domy-sprzedam-i-kupie/v1c9073p1'
const homeUrl = 'https://www.gumtree.pl'
const createUrl = (baseUrl, index) => baseUrl + index
const cracowUrls = [...Array(10)].map((_, i) => createUrl(cracowBaseUrl, i + 1))

const app = express()

function cleanPrice (price) {
  return price.replace(' ', '').replace('zł', '').replace(String.fromCharCode(160), '').replace('\n', '').replace('\t', '')
}

async function getDataFromUrl (addUrl) {
  const pageText = await fetch(addUrl).then(res => res.text())
  const dom = new Jsdom(pageText)
  const queryText = selector => dom.window.document.querySelector(selector).textContent
  const queryTextChild = selector => dom.window.document.querySelectorAll(selector)
  const result = {
    title: queryText('.myAdTitle'),
    description: queryText('.description'),
    price: cleanPrice(queryText('.value')),
    owner: queryTextChild('.value')[3].textContent,
    // dodaj id
    phone: dom.window.document.getElementsByClassName('reply_controls clearfix ')[0].getElementsByTagName('a')[0].getAttribute('href'),
    //isFlat: queryTextChild('.value')[4].textContent,
    //rooms: Number(queryTextChild('.value')[5].textContent.slice(0, 1)),
    //size: Number(queryTextChild('.value')[7].textContent),
    //name: queryText('.username').replace(/ .*/, '')
    //coordinates: dom.window.document.querySelector('.google-maps-link > img').src.match(/center\=(\d+\.\d+,\d+\.\d+)&/)[1].split(','),
    lat: dom.window.document.querySelector('.google-maps-link > img').src.match(/center\=(\d+\.\d+,\d+\.\d+)&/)[1].split(',')[0],
    lon: dom.window.document.querySelector('.google-maps-link > img').src.match(/center\=(\d+\.\d+,\d+\.\d+)&/)[1].split(',')[1]
    // long, lat (ale moze nawet nie ma potrzeby)
    // nr,ulica, tyle
  }
  return result
}

module.exports.cleanPrice = cleanPrice

async function urlsFromPage (pageUrl) {
  const siteText = await fetch(pageUrl).then(res => res.text())
  const dom = new Jsdom(siteText) // wirtualny dom ze strony którą pobrałem
  return Array
    .from(dom.window.document.querySelectorAll('.title > .href-link'))
    .map(node => homeUrl + node.href)
}

async function getDataFromUrls (urls) {
  return Promise.all(urls.map(getDataFromUrl))
}

// Main
const pagesUrlsToAdds = async (pagesUrls) => {
  const addUrls = (await Promise.all(cracowUrls.map(urlsFromPage)))
    .reduce((acc, cur) => [...acc, ...cur], [])
  return getDataFromUrls(addUrls)
}
r.connect({db: 'crawler'}).then((connection) => {
  (async () => {
    const adds = await pagesUrlsToAdds(cracowUrls)
    adds.forEach((add) => {
      r.db('crawler')
      .table('flats')
      .insert(add, { conflict: 'update' })
      .run(connection, (err, result) => { if (err) throw err })
    })
  })()

  console.log('nic')
  app.get('/api', (req, res) => {
    r.db('crawler').table('flats')
    .run(connection, (err, result) => {
      if (err) throw err
      console.log(result)
      result.toArray()
      .then(data => res.send(data))
    })
  })
})


app.get('/', (req, res) => res.sendFile(__dirname + '/index.html'))
app.get('/client.js', (req, res) => res.sendFile(__dirname + '/client.js'))
app.listen(3010, () => console.log('Example app listening on port 3010!'))

//     